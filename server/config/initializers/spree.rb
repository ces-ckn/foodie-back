# Configure Spree Preferences
#
# Note: Initializing preferences available within the Admin will overwrite any changes that were made through the user interface when you restart.
#       If you would like users to be able to update a setting with the Admin it should NOT be set here.
#
# Note: If a preference is set here it will be stored within the cache & database upon initialization.
#       Just removing an entry from this initializer will not make the preference value go away.
#       Instead you must either set a new value or remove entry, clear cache, and remove database entry.
#
# In order to initialize a setting do:
# config.setting_name = 'new value'
# 

Spree.config do |config|
  # Example:
  # Uncomment to stop tracking inventory levels in the application
   config.track_inventory_levels = false
  # Set Country name and Currency like this(Note: you will need to run 'rake db:seed' before this. Change country name in Spree::Country.find_by_name('United Kingdom') replace united kingdom to your desire one)
  config.currency = 'VND'
  country = Spree::Country.find_by_name('Viet Nam')
  config.default_country_id = country.id if country.present?
  # You can also set following options too.

  #config.site_name = "Foodie 2016"
end
Spree.user_class = "Spree::User"
