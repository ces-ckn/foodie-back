# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :rbenv_root, "/home/huy/.rbenv"
# env :MAILTO, "root"
# env :PATH, "#{rbenv_root}/shims:#{rbenv_root}/bin:/bin:/usr/bin"
# env :RBENV_VERSION, "2.3.0"
# set :environment, :development
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
# every 1.day, :at => '0:00 am' do
#   runner 'EmailSubscription.daily_email'
# end

every 2.minute do
	command "echo 'you can use raw cron syntax too'"

  runner 'Dish::EmailSubscription.daily_email'

end