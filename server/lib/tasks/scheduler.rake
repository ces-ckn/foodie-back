desc "This task is called by the Heroku scheduler add-on"

task :daily_email => :environment do
  Dish::EmailSubscription.daily_email
end

task :add_product_to_calendar => :environment do
  Dish::AvailableOn.add_product
  Dish::BoxProduct.add_dish_to_box
end