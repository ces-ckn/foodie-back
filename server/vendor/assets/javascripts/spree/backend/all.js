// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require spree/backend

//= require_tree .
//= require spree/backend/spree_dishes_management
//= require spree/backend/spree_dishes
//= require spree/backend/spree_users
//= require spree/backend/spree_users_management
//= require spree/backend/spree_email_subscriptions
//= require spree/backend/spree_boxes_management
//= require spree/backend/spree_orders
//= require spree/backend/spree_orders_management
//= require spree/backend/spree_paypal_express
//= require spree/backend/spree_advanced_reporting
//= require spree/backend/spree_bitpay
//= require spree/backend/spree_delivery_management
