// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require spree/frontend

//= require_tree .
//= require spree/frontend/spree_dishes_management
//= require spree/frontend/spree_dishes
//= require spree/frontend/spree_users
//= require spree/frontend/spree_users_management
//= require spree/frontend/spree_email_subscriptions
//= require spree/frontend/spree_boxes_management
//= require spree/frontend/spree_orders
//= require spree/frontend/spree_orders_management
//= require spree/frontend/spree_paypal_express
//= require spree/frontend/spree_advanced_reporting
//= require spree/frontend/spree_bitpay
//= require easyModal.js-master/jquery.easyModal
//= require spree/frontend/spree_delivery_management
